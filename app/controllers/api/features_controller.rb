module Api
  class FeaturesController < ApplicationController
    def index
      @features = Feature.all
      # render json:{ data:@features}
       render json:{ features:@features}

    end
    def create
      feature= Feature.create(name:params[:feature][:name])
      render json:{message: 'created',feature:feature},status:201
      #redirect_to @features
    end
    def show
      @feature = Feature.includes(:test).find(params[:id])
      render :json => @feature.to_json(:include => [:test])

    end
  end
end