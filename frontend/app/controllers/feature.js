import Controller from '@ember/controller';

export default Controller.extend({
  actions:{
    createFeature(){
      let newFeature = this.get('newFeature');
      let newRecord = this.store.createRecord('feature',{ name: newFeature});
      newRecord.save()
    }
  }
});
