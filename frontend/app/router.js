import EmberRouter from '@ember/routing/router';
import config from './config/environment';

const Router = EmberRouter.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('feature');
  // this.route('tests', {path:'feature/:feature_id/tests'});
  this.route('test', {path:'feature/:feature_id'})
});

export default Router;
