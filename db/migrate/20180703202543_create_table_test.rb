class CreateTableTest < ActiveRecord::Migration
  def change
    create_table :tests do |t|
      t.integer :feature_id
      t.string :name
      t.string :status

      t.timestamps
    end
    add_foreign_key :tests, :features
  end
end
