# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
Feature.create([ {name: 'feature 1'},{name: 'feature 2'} ])
Test.create( [ {feature_id: 1, name:'test 1', status:'undefined'},{feature_id: 2,name:'test1',status:'pased'} ])